package com.example.tamjid.lifxv2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    String allOn="2a:00:00:34:b4:3c:f0:84:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01:0d:00:00:00:00:00:00:00:00:75:00:00:00:ff:ff:e8:03:00:00";
    String allOff="2a:00:00:34:b4:3c:f0:84:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01:0d:00:00:00:00:00:00:00:00:75:00:00:00:00:00:e8:03:00:00";
    int serverPort=56700;




    byte[] ao=byteBuilder(allOn);
    byte[] aof=byteBuilder(allOff);
    DatagramPacket allOnPack=new DatagramPacket(ao,allOn.length(),InetAddress.getByName("255.255.255.255"),serverPort);
    DatagramPacket allOffPack=new DatagramPacket(aof,allOff.length(),InetAddress.getByName("255.255.255.255"),serverPort);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public byte[] byteBuilder(String hex){
        String command=hex;
        byte byt_cmd[]=new byte[42];
        char[] byt=new char[2];
        for(int i=0,j=0,k=0;i<command.length();i++){
            char c=command.charAt(i);
            if(c==':'){
                j=0;
                BigInteger big=new BigInteger(String.valueOf(byt),16);
                byte x=(byte) Integer.parseInt(String.valueOf(big));
                byt_cmd[k++]=x;
            }
            else{
                byt[j]=c;
                j++;
            }
        }
        return byt_cmd;
    }
}